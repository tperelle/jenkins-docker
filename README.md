# jenkins-docker

## Config
* Configure `JAVA_OPTS` and `JENKINS_OPTS` in the Dockerfile. 
* Configure Jenkins parameters in the file `config.xml`
* Configure plugins to install in the file `plugins.txt`
* Pass `USERNAME` and/or `PASSWORD` build parameters to adapt username and password of the first admin account

## Run
To run Jenkins :
```
docker image built -t jenkins/jenkins-test .
docker container exec -d -p 8080:8080 jenkins/jenkins-test
```

## Access
Access Jenkins at `http://localhost:8080`

