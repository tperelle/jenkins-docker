FROM jenkins/jenkins

ARG USERNAME=admin
ARG PASSWORD=admin

ENV JAVA_OPTS="-Djenkins.install.runSetupWizard=false"
ENV JENKINS_OPTS="--argumentsRealm.passwd.${USERNAME}=${PASSWORD} --argumentsRealm.roles.${USERNAME}=admin"
ENV BUILD_URL="localhost:8080"

COPY plugins.txt /usr/share/jenkins/ref/plugins.txt
COPY config.xml ${JENKINS_HOME}/config.xml
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt

